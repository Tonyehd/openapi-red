// version === node version
const clientUpdate = (version, node) => {
  // lower than 2.0.0
  if (version.aggregated < version.parse('2.0.0').aggregated) {
    if (typeof node.urlType === 'undefined') node.urlType = 'str'
    if (typeof node.serverType === 'undefined') node.serverType = 'custom'
  }
  return node
}

module.exports = {
  clientUpdate
}
