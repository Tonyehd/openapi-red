const Swagger = require('swagger-client')
const util = require('util')

module.exports = function (RED) {
  // syncronous evaluateNodeProperty is depreceated with Node-RED 3.1
  // https://github.com/flowforge/flowforge-nr-dashboard/issues/99
  const evalNodeProp = util.promisify(RED.util.evaluateNodeProperty)
  const evaluateNodeProp = async (value, type, thiz, msg) => {
    try {
      // using try/catch to return a more user friendly error
      const result = await evalNodeProp(value, type, thiz, msg)
      return result
    } catch (e) {
      console.error(e)
      throw new Error(`[openApi-red] Could not evaluate value "${value}" from type "${type}".`)
    }
  }

  function openApiRed (config) {
    RED.nodes.createNode(this, config)
    const node = this

    // openApi-red >= v.2.0.0 uses parameter object instead of array
    const restructureParamFromArray = (parameters) => {
      console.warn('[openApi-red] Warning: Depreceated parameter format. Please use an object instead of an array.')
      const newParameterObject = {}
      parameters.forEach(parameter => {
        newParameterObject[parameter.name] = parameter
      })
      return newParameterObject
    }

    // return -1 if not found
    const findOutputNumber = (response) => {
      const responseCode = response.code || response.statusCode || response.status || ''
      let outputNumber
      if (config.outputStyle === 'compact') {
        outputNumber = config.responseOutputLabels.findIndex(label => label.code === (responseCode?.toString()?.substring(0, 1) + 'xx'))
      } else if (config.outputStyle === 'each response') {
        outputNumber = config.responseOutputLabels.findIndex(label => Number(label.code) === responseCode)
      }
      if (outputNumber === -1 && response.ok) {
        // check for unspecified successful output. this should always be index === 0
        // This is only available if no successful output was defined. Else it will go to undefined responses.
        outputNumber = config.responseOutputLabels.findIndex(label => label.code === 'successful' || label.code === 'all')
      }

      // still no defined output found, exist an undefined response output?
      if (outputNumber === -1 && config.errorHandling === 'other output') {
        outputNumber = config.responseOutputLabels.findIndex(label => label.code === 'undefinedResponses')
      }
      return outputNumber
    }

    const setMessage = (msg, response) => {
      if (typeof response === 'string') {
        msg.payload = response
      } else if (config.responseAsPayload) {
        // legacy mode
        msg.payload = response
      } else {
        // More about the duplicates: https://github.com/swagger-api/swagger-js/blob/96d261987700297a472db5587c0a8c769095d73e/src/http/index.js#L115
        delete response.obj // Duplicate of response.body
        delete response.data // Duplicate of response.text
        msg.payload = response.body
        msg.response = response
        delete msg.response.body
      }
    }

    node.on('input', async function (msg, send, done) {
      send = send || function () { node.send.apply(node, arguments) }

      const sendError = (e) => {
        node.status({ fill: 'red', shape: 'dot', text: 'Error code: ' + (e.code || e.statusCode || e.status || 'Unknown code') })
        const errorMsg = `${e.status || e.response?.body?.code || ''} ${e.message} ${e.response?.body?.message ? '- ' + e.response.body.message : ''}`
        setMessage(msg, e.response || e.message)
        if (config.outputStyle === 'classic') {
          if (config.errorHandling === 'other output') {
            send([null, msg])
            done()
          } else if (config.errorHandling === 'throw exception') {
            done(errorMsg)
          } else {
            send(msg)
            done()
          }
        } else if (config.outputStyle === 'each response' || config.outputStyle === 'compact') {
          // check for an existing response output (includes check for error handling "other output")
          const outputNumber = findOutputNumber(e)
          if (outputNumber > -1) {
            const msgArray = Array(outputNumber).fill(null)
            msgArray.push(msg)
            send(msgArray)
            done()
          } else {
            // if no output found, throw expection
            done(e)
          }
        }
      }
      const buildEditorValue = async (param, alreadyNested = false) => {
        let value
        if (param.isActive) {
          if (param.type.startsWith('editor')) {
            value = alreadyNested ? {} : { [param.name]: {} }
            for await (const paramParam of Object.values(param.parameters || {})) {
              if (paramParam.isActive) {
                if (alreadyNested) {
                  value[paramParam.name] = await buildEditorValue(paramParam, true)
                } else {
                  value[param.name][paramParam.name] = await buildEditorValue(paramParam, true)
                }
              }
            }
          } else if (param.type === 'array') {
            value = []
            for await (const arrayParam of param.value) {
              value.push(await buildEditorValue(arrayParam, true))
            }
          } else if (param.type === 'select') {
            try {
              value = JSON.parse(param.value)
            } catch (e) {
              // undefined (if somebody really want to make that selectable....) or other error, send at least the string value
              value = param.value
            }
          } else {
            value = await evaluateNodeProp(param.value, param.type, this, msg)
          }
        }
        return value
      }

      const configNode = RED.nodes.getNode(config.configUrlNode) || {}
      let parameters = {}
      // eslint-disable-next-line
      let requestBody = undefined
      if (msg.openApi?.parameters) {
        parameters = msg.openApi.parameters
        if (Array.isArray(parameters)) {
          parameters = restructureParamFromArray(parameters)
        }
      } else {
        if (Array.isArray(config.parameters)) {
          config.parameters = restructureParamFromArray(config.parameters)
        }
        try {
          const parameterKeys = Object.keys(config.parameters)
          // forEach cannot be async...
          for await (const pKey of parameterKeys) {
            const parameter = config.parameters[pKey]
            if (parameter.isActive || parameter.required) {
              let parameterValue
              if (parameter.type.startsWith('editor')) {
                // recursive build of object -> e.g. returns { body: { id: 123 } }
                // but it must be set to parameters.body (below) -> only the value of the object is needed
                parameterValue = await buildEditorValue(parameter)
                parameterValue = parameterValue[parameter.name] // remove first object level ({Request body: {} })
              } else if (parameter.type === 'array') {
                if (!Array.isArray(parameter.value)) {
                  throw new Error('[openApi-red] Parameter "' + parameter.name + '" is from type array, but it\'s value not.')
                }
                parameterValue = []
                for await (const obj of parameter.value) {
                  parameterValue.push(await evaluateNodeProp(obj.value, obj.type, this, msg))
                }
              } else {
                parameterValue = await evaluateNodeProp(parameter.value, parameter.type, this, msg)
              }
              // query input can't be object, swagger.js should handle this but does not (https://github.com/swagger-api/swagger-js/blob/master/docs/usage/http-client.md#query-support)
              // arrays still work and will be set to "...tag=val1&tag=val2"
              if (typeof parameterValue === 'object' && !Array.isArray(parameterValue) && parameter.in === 'query') {
                parameterValue = JSON.stringify(parameterValue)
              }
              if (parameter.name === 'Request body') {
                requestBody = parameterValue
              } else {
                parameters[parameter.name] = parameterValue
              }
            }
          }
        } catch (e) {
          console.log('[openApi-red] Error creating parameters')
          sendError(e)
          return
        }
      }

      // fallback if no content type can be found
      let requestContentType = 'application/json'
      if (config.requestContentType) requestContentType = config.requestContentType
      const opData = config.operationData
      // try to get it if source was unavailable on startup (e.g. NodeRed creates specification or server/shuttle was not ready yet)
      let spec
      try {
        spec = configNode?.openApiSpecification() || null
        if (!spec) {
          await configNode.loadOpenApiSpec({
            id: configNode.id,
            apiSource: configNode.url,
            sourceType: configNode.urlType,
            serverUrl: configNode.server,
            serverType: configNode.serverType,
            devMode: configNode.devMode
          })
          spec = configNode.openApiSpecification()
        }

        // upgrade to v.2 needs operation method (node was not opened yet after upgrade)
        if (!opData.method && opData.hasOperationId && opData.id) {
          opData.method = Object.keys(spec.paths[opData.path] || {}).find(method => spec.paths[opData.path][method]?.operationId === opData.id)
        }
        // resolve if neccessary (this is neccessary once, if the node editor was not opened)
        if (!spec.paths[opData.path][opData.method]['x-openApi-red-resolved']) {
          await Swagger.resolveSubtree(spec, ['paths', opData.path, opData.method])
          spec.paths[opData.path][opData.method]['x-openApi-red-resolved'] = true
        }
      } catch (e) {
        // ignore error -> do not let NR crash
        console.log(e)
      }
      if (!spec) {
        return sendError(new Error('No openApi specification found. Please check the config node.'))
      }

      const requestSettings = {
        // preferred use is operationId. If not available use pathname + method
        operationId: opData.hasOperationId ? opData.id : undefined,
        pathName: opData.hasOperationId ? undefined : opData.path,
        method: opData.hasOperationId ? undefined : opData.method,
        parameters,
        requestBody,
        requestContentType,
        // if available put token for auth
        requestInterceptor: (req) => {
          if (msg.openApiToken) req.headers.Authorization = 'Bearer ' + msg.openApiToken
          if (msg.headers) req.headers = Object.assign(req.headers || {}, msg.headers)
          if (!config.keepAuth) {
            delete msg.openApiToken
            delete msg.headers
          }
        }
      }
      if (config.responseContentType) {
        requestSettings.responseContentType = config.responseContentType
      }

      if (spec.swagger?.startsWith('2')) {
        if (configNode.server) {
          requestSettings.contextUrl = configNode.server
        }
      } else {
        // will only work with openApi v3
        // important: the custom server must be set into the specs, else it will be ignored
        if (['msg', 'flow', 'global'].includes(configNode.serverType)) {
          requestSettings.server = await evaluateNodeProp(configNode.server, configNode.serverType, this, msg)
          if (!spec.servers.find(server => server.url === requestSettings.server)) {
            spec.servers.push({ url: requestSettings.server })
          }
        } else {
          requestSettings.server = configNode.server || spec.openApiRed?.defaultServer // this option is already set into the specs
        }
      }

      if (node.devMode) {
        const agent = require('https').Agent({
          rejectUnauthorized: false
        })
        requestSettings.http = (request) => Swagger.http({ ...request, agent })
      }

      node.status({ fill: 'yellow', shape: 'dot', text: 'Retrieving...' })
      try {
        const response = await Swagger.execute({ spec, ...requestSettings })
        node.status({})
        setMessage(msg, response)
        if (config.outputStyle === 'classic') {
          send(msg)
          done()
        } else {
          const outputNumber = findOutputNumber(response)
          if (outputNumber > -1) {
            const msgArray = Array(outputNumber).fill(null)
            msgArray.push(msg)
            send(msgArray)
          } else {
            // No output found -> throw exception
            sendError(response)
          }
        }
      } catch (e) {
        // invalid input or server returns error
        return sendError(e)
      }
    })
  }
  RED.nodes.registerType('openApi-red', openApiRed)
}
